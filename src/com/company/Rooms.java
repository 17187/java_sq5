package com.company;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Rooms {
    int id, r,floor;
    String room;
    boolean status;
    double price;

    public Rooms(String room, boolean status, double price) {
        this.room = room;
        this.status = status;
        this.price = price;
    }

    public Rooms(int id, int r, int floor, String room, boolean status, double price) {
        this.id = id;
        this.r = r;
        this.floor = floor;
        this.room = room;
        this.status = status;
        this.price = price;
    }

    public Rooms() {
    }


}
