package com.company;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SQL_Connection {
    Connection getConnection(){
        String URL = "jdbc:postgresql://34.87.144.90:80/postgres";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(URL, "postgres", "seanghorn");
            return conn;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }

}
