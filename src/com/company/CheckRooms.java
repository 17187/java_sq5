package com.company;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class CheckRooms extends JFrame implements ActionListener {
    SQL_Connection sql_connection = new SQL_Connection();
    SQL_QUERY sql_query = new SQL_QUERY();
    Statement statement;
    ResultSet resultSet;
    void checkRoom(){
        JTable j;
        ArrayList<Rooms> rooms = new ArrayList<>();
        setTitle("Check All Rooms");
        String [][] data = null;
        //Get All Room Information
        try {
            statement = sql_connection.getConnection().createStatement();
            System.out.println(statement);
            resultSet = statement.executeQuery(sql_query.SELECT_ALL_IN_ROOM);
            System.out.println(resultSet);
            while (resultSet.next()){
                System.out.println(resultSet.getInt(1));
                int id = resultSet.getInt(1);
                String room_name = resultSet.getString(2);
                int room_index = resultSet.getInt(3);
                int floor = resultSet.getInt(4);
                boolean status = resultSet.getBoolean(5);
                double price = resultSet.getDouble(6);
                Rooms room = new Rooms(id,room_index,floor,room_name,status,price);
                rooms.add(room);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        data = new String[rooms.size()][5];
        System.out.println("room:"+rooms.get(0).room);
        for (int i=0;i<rooms.size();i++){
            data[i][0] = rooms.get(i).room;
            data[i][1] = String.valueOf(rooms.get(i).r);
            data[i][2] = String.valueOf(rooms.get(i).floor);
            data[i][3] = String.valueOf(rooms.get(i).price);
            if(rooms.get(i).status == true){
                data[i][4] = "Unavailable";
            }else {
                data[i][4] = "Available";
            }
        }

        // Column Names


        String[] columnNames = { "Name", "Room", "Floor","Price", "Status" };

        // Initializing the JTable
        j = new JTable(data, columnNames);
        j.setBounds(80, 40, 100, 300);

        // adding it to JScrollPane
        JScrollPane sp = new JScrollPane(j);
        sp.setBounds(80, 40, 100, 300);
        add(sp);
        // Frame Size
        setSize(600, 500);
        // Frame Visible = true
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
