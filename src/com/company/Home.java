package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Home extends JFrame implements ActionListener {
    JButton btnCheckRoom, btnCheckin, btnCheckout,btnCheckGuest;
    void home(){
        setVisible(true);
        setSize(700, 700);
        setLayout(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Geusthouse Management");
        JLabel ltitle;
        ltitle = new JLabel("Home Page :");
        ltitle.setForeground(Color.darkGray);
        ltitle.setFont(new Font("Serif", Font.BOLD, 20));
        ltitle.setBounds(80, 30, 400, 30);
        add(ltitle);
        btnCheckRoom = new JButton("Check All Room");
        btnCheckGuest = new JButton("Check All Guest");
        btnCheckin = new JButton("Check In");
        btnCheckout = new JButton("Check Out");
        btnCheckRoom.addActionListener((ActionListener) this);
        btnCheckGuest.addActionListener((ActionListener) this);
        btnCheckin.addActionListener((ActionListener) this);
        btnCheckout.addActionListener((ActionListener) this);
        btnCheckin.setBounds(80, 70, 100, 30);
        btnCheckout.setBounds(280, 70, 100, 30);
        btnCheckGuest.setBounds(80, 110, 150, 30);
        btnCheckRoom.setBounds(280, 110, 150, 30);
        add(btnCheckRoom);
        add(btnCheckout);
        add(btnCheckin);
        add(btnCheckGuest);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == btnCheckin){
            new Checkin().Registration();
        }
        else if(e.getSource() == btnCheckout){
            new checkout();
        }
        else if(e.getSource() == btnCheckRoom){
            new CheckRooms().checkRoom();
        }
        else if(e.getSource() == btnCheckGuest){

        }
    }
}
