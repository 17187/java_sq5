package com.company;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.*;
import java.util.ArrayList;
import java.util.Properties;

public class Checkin extends JFrame implements ActionListener {
    JLabel ltitle, lName, lGender, lDOB, lRoom, lPhone, lPrice,lCheckin;
    JTextField txtName, txtGender, txtDOB, txtPhoneNumber, txtPrice;
    JButton btn1, btn2;
    JDatePickerImpl dob,dateCheckin;
    UtilDateModel dateModel,model;
    JDatePanelImpl datePanel,checkinPanel;
    JComboBox cGender,cRooms;
    int room_id;
    double price;
    SQL_QUERY sql_query = new SQL_QUERY();
    SQL_Connection sql_connection = new SQL_Connection();
    ArrayList<Rooms> rooms = new ArrayList<>();
    PreparedStatement statement ;
    ResultSet resultSet;
    String queryStr;
    void Registration()
    {
        setVisible(true);
        setSize(700, 700);
        setLayout(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Geusthouse Management");
        //Title set up
        ltitle = new JLabel("Form Check in :");
        ltitle.setForeground(Color.darkGray);
        ltitle.setFont(new Font("Serif", Font.BOLD, 20));
        ltitle.setBounds(100, 30, 400, 30);
        add(ltitle);

         //Information to Check In
        lName = new JLabel("Name:");
        txtName = new JTextField();
        lName.setBounds(80, 70, 200, 30);
        txtName.setBounds(300, 70, 200, 30);
        add(lName);
        add(txtName);

        lGender = new JLabel("Gender:");
        String sex[] = { "Male", "Female", "Other" };
        cGender = new JComboBox(sex);
        lGender.setBounds(80, 110, 200, 30);
        cGender.setBounds(300, 110, 200, 30);
        add(lGender);
        add(cGender);

        // Add Date to Lib
        dateModel = new UtilDateModel();
        Properties p = new Properties();
        p.put("text.today", "Today");
        p.put("text.month", "Month");
        p.put("text.year", "Year");
        datePanel = new JDatePanelImpl(dateModel,p);
        dob = new JDatePickerImpl(datePanel, new DateLabelFormatter());


        lDOB = new JLabel("Date of Birth");
        txtDOB = new JTextField();
        lDOB.setBounds(80, 150, 200, 30);
        dob.setBounds(300, 150, 200, 30);
        //txtDOB.setBounds(300, 230, 200, 30);
        add(lDOB);
        add(dob);

        lPhone = new JLabel("Phone No:");
        txtPhoneNumber = new JTextField();
        lPhone.setBounds(80, 190, 200, 30);
        txtPhoneNumber.setBounds(300, 190, 200, 30);
        add(lPhone);
        add(txtPhoneNumber);

        lRoom = new JLabel("Room:");
        getRoom();
        String room_nums [] = new String[rooms.size()];
        System.out.println(rooms.size());
        for (int i =0;i<rooms.size();i++){
            room_nums[i] = rooms.get(i).room;
        }
        cRooms = new JComboBox(room_nums);
        lRoom.setBounds(80, 230, 200, 30);
        cRooms.setBounds(300, 230, 200, 30);
        add(lRoom);
        add(cRooms);
        lPrice = new JLabel("Price:");
        txtPrice = new JTextField();
        lPrice.setBounds(80, 270, 200, 30);
        txtPrice.setBounds(300, 270, 200, 30);
        add(lPrice);
        add(txtPrice);
        cRooms.addItemListener(new ItemListener() {
            String rooom_selected;
            public void itemStateChanged(ItemEvent arg0) {
                rooom_selected = cRooms.getSelectedItem().toString();
                try {
                    statement = sql_connection.getConnection().prepareStatement(sql_query.SELECT_ROOM_PRICE);
                    statement.setString(1,rooom_selected);
                    resultSet = statement.executeQuery();
                    while (resultSet.next()){

                        System.out.println("id:"+resultSet.getInt(1));
                        room_id = resultSet.getInt(1);
                        System.out.println("price:"+resultSet.getDouble(2));
                        price = resultSet.getDouble(2);
                        txtPrice.setText(String.valueOf(resultSet.getDouble(2)));
                    }

                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }


        });
        lCheckin = new JLabel("Check in Date:");
        model = new UtilDateModel();
        checkinPanel = new JDatePanelImpl(model,p);
        dateCheckin = new JDatePickerImpl(checkinPanel, new DateLabelFormatter());
        lCheckin.setBounds(80, 310, 200, 30);
        dateCheckin.setBounds(300, 310, 200, 30);
        add(lCheckin);
        add(dateCheckin);
        btn1 = new JButton("Submit");
        btn2 = new JButton("Clear");
        btn1.addActionListener((ActionListener) this);
        btn2.addActionListener((ActionListener) this);
        btn1.setBounds(80, 350, 100, 30);
        btn2.setBounds(230, 350, 100, 30);
        add(btn1);
        add(btn2);
    }
    public void actionPerformed(ActionEvent e)
    {
        if (e.getSource() == btn1)
        {
            String name= txtName.getText();
            String gender = cGender.getSelectedItem().toString();
            String date = dob.getJFormattedTextField().getText();
            String contact = txtPhoneNumber.getText();
            String checkinDate = dateCheckin.getJFormattedTextField().getText();
            boolean check;
            System.out.println(room_id+":"+name+":"+gender+":"+date+":"+contact+":"+price+":"+checkinDate);
                try
                {
                    statement = sql_connection.getConnection().prepareStatement(sql_query.INSERT_INTO_GUEST);
                    statement.setString(1, name);
                    statement.setString(2, gender);
                    statement.setString(3, date);
                    statement.setString(4, contact);
                    statement.setInt(5, room_id);
                    statement.setString(6,checkinDate);
                    statement.execute();
                    check = true;
                    System.out.println(check);
                    statement.close();
                    sql_connection.getConnection().close();
                    txtName.setText("");
                    cGender.setSelectedIndex(0);
                    txtDOB.setText("");
                    txtPhoneNumber.setText("");
                    txtPrice.setText("");
                    if (check==true){
                        // Update room status
                    statement = sql_connection.getConnection().prepareStatement(sql_query.UPDATE_ROOM_STATUS_BY_ID);
                    statement.setBoolean(1,true);
                    statement.setInt(2,room_id);
                    statement.executeUpdate();
                    System.out.println("statement"+statement.toString());
                    }
                }
                catch (Exception ex)
                {
                    System.out.println("Error");
                    System.out.println(ex.getMessage());
                }
            }
            if(e.getSource()==btn2){
                txtName.setText("");
                cGender.setSelectedIndex(0);
                txtDOB.setText("");
                txtPhoneNumber.setText("");
                txtPrice.setText("");
            }
        }
    void getRoom(){
        try {

             queryStr= sql_query.SEFLECT_AVALABLE_ROOM;
            Statement stmt = sql_connection.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(queryStr);
            while(rs.next()) {
                String room = String.valueOf(rs.getString(1));
                boolean status = Boolean.valueOf(rs.getBoolean(2));
                double price = Double.valueOf(rs.getDouble(3));
                Rooms r = new Rooms(room,status,price);
                rooms.add(r);
                System.out.println(r.room);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();

        }
    }

}

